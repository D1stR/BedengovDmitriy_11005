//Рекурсивно вычислить функцию Аккермана от двух целых чисел M, N > 0.

public class homework74 {
    public static void main(String[] args) {
        int m = 3;
        int n = 3;
        System.out.println(akkerman(m, n));
    }
    public static int akkerman(int m, int n) {
        if (m == 0)
            return n + 1;
        if (n == 0)
            return akkerman(m - 1,1);
        return akkerman(m - 1, akkerman(m,n - 1));
    }
}
