//Рекурсивно вычислить число Фибоначчи с номером N.

public class homework73 {
    public static void main(String[] args) {
        int n = 22 ;
        System.out.println(fibonachi(n));
    }
    public static int fibonachi(int n) {
        if (n < 1)
            return 0;
        if (n < 2)
            return 1;
        return fibonachi(n - 1) + fibonachi(n - 2);
    }
}
