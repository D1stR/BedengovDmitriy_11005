//Рекурсивно посчитать произведение чисел от 1 до N.

public class homework71 {
    public static void main(String[] args) {
        int n = 8;
        System.out.print("Result: " + proizvedenie(n));
    }
    public static int proizvedenie(int n) {
        if (n < 1)
            return 1;
        return n * proizvedenie(n - 1);
    }
}
