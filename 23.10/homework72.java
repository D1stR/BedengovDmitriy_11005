//Рекурсивно вывести целые числа от A до B (A < B)

public class homework72 {
    public static void main(String[] args) {
        int a = 2;
        int b = 20;
        AtoB(a, b);
    }
    public static void AtoB(int a, int b) {
        if (b < a)
            return;
        System.out.println(a);
        AtoB(a+1, b);
    }
}
