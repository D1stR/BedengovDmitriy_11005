//Написать метод, который находит максимальное значение массива.
import java.util.Scanner;
public class homework63 {
  public static int Max(int[] a){
      System.out.println("Вывод: ");
      int max = Integer.MIN_VALUE;
      for (int i = 0; i < a.length; i++){
          if (a[i]> max){
              max = a[i];
          }
      }
      return max;
  }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество чисел");
        int n = scanner.nextInt();
        int arr[] = new int[n];
        System.out.println("Введите числа: ");
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        System.out.println(Max(arr));
    }

}
