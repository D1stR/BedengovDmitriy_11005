//Написать метод, который принимает два массива и “склеивает” их (для массивов {1,3,5} и {2,4,6} будет результат {1,3,5,2,4,6}).
import java.util.Scanner;
public class homework64 {
    public static int[] Skleivaet(int[] a1, int[] a2){
        int[] result = new int[(a1.length + a2.length)];
        for (int i = 0; i < a1.length; i++){
            result[i] = a1[i];
        }
        for (int i = a1.length; i < result.length; i++){
            result[i] = a2[i-a1.length];
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество чисел для 1ого массива: ");
        int n = scanner.nextInt();
        int a1[] = new int[n];
        System.out.println("Введите числа 1ого массива: ");
        for (int i = 0; i < n; i++) {
            a1[i] = scanner.nextInt();
        }
        System.out.println("Введите количество чисел для 2ого массива: ");
        int k = scanner.nextInt();
        int a2[] = new int[k];
        System.out.println("Введите числа 2ого массива: ");
        for (int i = 0; i < k; i++) {
            a2[i] = scanner.nextInt();
        }
        int[] otvet = Skleivaet(a1, a2);
        System.out.println("Ответ: ");
        for (int i = 0; i < otvet.length; i++){
            System.out.print(" " + otvet[i]);
        }
    }
}
