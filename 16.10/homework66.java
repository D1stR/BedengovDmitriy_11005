//Написать метод, который принимает на вход две матрицы и возвращает их сумму.
import java.util.Scanner;
public class homework66 {
    public static int[][] Cymma(int[][] m1, int[][] m2){
        int[][] otvet = new int[m1.length][m1[0].length];
        for (int i = 0; i < otvet.length; i++){
            for (int j = 0; j < otvet[i].length; j++){
                otvet[i][j] = m1[i][j] + m2[i][j];
            }
        }
        return otvet;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер матриц : ");
        int n = scanner.nextInt();
        int m1[][] = new int [n][n];
        System.out.print("Матрица 1:  ");
        System.out.println();
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                m1[i][j] =  (int)Math.round(Math.random()*10);
            }
        }
        for ( int i = 0;i < n; i++){
            for (int j = 0;j < n; j++)
                System.out.print(m1[i][j]+" ");
            System.out.println();
        }
        int m2[][] = new int [n][n];
        System.out.print("Матрица 2:  ");
        System.out.println();
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                m2[i][j] =  (int)Math.round(Math.random()*10);
            }
        }
        for ( int i = 0;i < n; i++){
            for (int j = 0;j < n; j++)
                System.out.print(m2[i][j]+" ");
            System.out.println();
        }
        int[][] Cymma1 = Cymma(m1,m2);
        System.out.println("Сумма матриц: ");

        for (int i = 0; i < Cymma1.length; i++){
            for (int j = 0; j < Cymma1[i].length;j++){
                System.out.print(Cymma1[i][j] + " ");
            }
            System.out.println("");
        }
    }
}
