//Написать метод, который выводит строку в обратном порядке.
import java.util.Scanner;
public class homework62 {
        public static void StrokaNaoborot(String s){
            char[] a = s.toCharArray();
            for (int i = a.length-1; i >= 0; i-- ){
                System.out.print(a[i]);
            }
        }
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            String s = scanner.nextLine();
            StrokaNaoborot(s);
        }

}
