//Написать метод, который принимает на вход матрицу и транспонирует её.
import java.util.Scanner;
public class homework65 {
    public static int[][] Transponirovanie(int[][] matrix) {
        int[][] otvet = new int[matrix[0].length][matrix.length];
        for (int i = 0; i < otvet.length; i++) {
            for (int j = 0; j < otvet[0].length; j++) {
                otvet[i][j] = matrix[j][i];
            }
        }
        return otvet;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер матрицы: ");
        int n = scanner.nextInt();
        int matrix[][] = new int [n][n];
        System.out.print("Матрица:  ");
        System.out.println();
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                matrix[i][j] =  (int)Math.round(Math.random()*10);
            }
        }
        for ( int i = 0;i < n; i++){
            for (int j = 0;j < n; j++)
                System.out.print(matrix[i][j] + " ");
            System.out.println();
        }
        int[][] Transponirovanie1 = Transponirovanie(matrix);
        System.out.println("Транспонированная матрица: ");
        for (int i = 0; i < Transponirovanie1.length; i++) {
            for (int j = 0; j < Transponirovanie1[0].length; j++) {
                System.out.print(Transponirovanie1[i][j] + " ");
            }
            System.out.println("");
        }
    }
}
