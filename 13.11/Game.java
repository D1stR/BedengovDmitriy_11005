//Реализовать модель игры – игроки последовательно друг другу наносят удары силой от 1 до 9, при этом у того, кого ударили, из очков здоровья (health points, hp) отнимается сила удара. Игра заканчивается, когда hp одного из игроков стало = 0. В задаче должны быть классы Игрок и Игра, в которой весь процесс происходит. У каждого игрока есть имя и hp. Сила удара каждого игрока на каждом шаге вводится из консоли. Процесс игры должен принадлежать объекту класса Игра.
import java.util.Scanner;
public class Game {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int damage = 0;
        System.out.println("Введите имя 1ого игрока: ");
        Player player1 = new Player(in.nextLine(), 10);
        System.out.println("Введите имя 2ого игрока: ");
        Player player2 = new Player(in.nextLine(), 10);


        while (player1.getHp() > 0 && player2.getHp() > 0) {
            System.out.println("Ходит "+ player1.getName());
            System.out.print("Введите силу удара от 0 до 9: ");
            damage = in.nextInt();
            player1.impact(damage);
            System.out.print("Оставшееся здоровье:");
            System.out.println(player1.getHp());

            System.out.println("Ходит "+ player2.getName());
            System.out.print("Введите силу удара от 0 до 9: ");
            damage = in.nextInt();
            player2.impact(damage);
            System.out.print("Оставшееся здоровье:");
            System.out.println(player2.getHp());
        }

        if (player1.getHp() <= 0) {
            System.out.println("Победил "+ player1.getName());
        }

        if (player2.getHp() <= 0) {
            System.out.println("Победил " + player2.getName());
        }

    }
}
