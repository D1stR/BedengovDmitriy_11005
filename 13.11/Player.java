public class Player {
    private String name;
    private int hp;

    public Player(String name, int hp) {
        this.name = name;
        this.hp = hp;
    }

    public String getName() {
        return name;
    }

    public int getHp() {
        return hp;
    }

    public void impact(int damage) {
        int chanceToImpact = 0;
        int interest = (damage + 1) * 10;
        chanceToImpact = (int) Math.floor(Math.random() * 100) + 1;
        if (chanceToImpact > interest) {
            System.out.println(getName()+" попал");
            this.hp -= damage;
            if (this.hp < 0) {
               this.hp = 0;
            }
        } else {
            System.out.println(getName()+" не попал");
            this.hp -= 0;
        }
    }
}
