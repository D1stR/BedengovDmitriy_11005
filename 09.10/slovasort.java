//Дана строка из нескольких слов, разделённых пробелами. Отсортировать слова в ней по алфавиту, т.е. из “bca cba cab acb” получить “acb bca cab cba”. Оценить сложность полученного алгоритма по времени и по памяти.

import java.util.Arrays;
import java.util.Scanner;

public class slovasort {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество слов:");
        int n = scanner.nextInt();
        String a[] = new String[n];
        System.out.println("Введите слова:");
        for (int i = 0; i < a.length; i++)
        {
            a[i] = scanner.nextLine();
        }
        String t = "";
        for (int i = 0; i < a.length; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (a[j].compareTo(a[i]) < 0) {
                    t = a[i];
                    a[i] = a[j];
                    a[j] = t;
                }
            }
        }
        for (String val : a) {
            System.out.println(val);
        }

    }
}
