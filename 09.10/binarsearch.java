// В отсортированном массиве найти элемент с помощью бинарного поиска

import java.util.Arrays;
import java.util.Scanner;

public class binarsearch {

    public static void main(String args[]) {
        int c, num, item, a[], first, last;


        Scanner input = new Scanner(System.in);
        System.out.println("Количество элементов: ");
        num = input.nextInt();

        a = new int[num];

        System.out.println("Введите " + num + " чисел");


        for (c = 0; c < num; c++)
            a[c] = input.nextInt();


        Arrays.sort(a);

        System.out.println("Введите число для поиска: ");
        item = input.nextInt();
        first = 0;
        last = num - 1;


        binarySearch(a, first, last, item);
    }

    public static void binarySearch(int[] a, int first, int last, int item) {
        int pos;
        int Count = 1;

        pos = (first + last) / 2;

        while ((a[pos] != item) && (first <= last)) {
            Count++;
            if (a[pos] > item) {
                last = pos - 1;
            } else {
                first = pos + 1;
            }
            pos = (first + last) / 2;
        }
        if (first <= last) {
            System.out.println(item + " является " + ++pos + " элементом в массиве");
            System.out.println("Метод бинарного поиска нашел число после " + Count +
                    " сравнений");
        } else {
            System.out.println("Элемент не найден в массиве. Метод бинарного поиска закончил работу после "
                    + Count + " сравнений");
        }
    }
}

