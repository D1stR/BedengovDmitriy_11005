

public class Triangle extends AbstractClassShape {

    private int heightOfTriangle;
    private int baseOfTriangle;

    Triangle(int heightOfTriangle, int baseOfTriangle){
        this.heightOfTriangle = heightOfTriangle;
        this.baseOfTriangle = baseOfTriangle;
    }

    @Override
    double getArea() {
        return (heightOfTriangle*baseOfTriangle)/2;
    }
}