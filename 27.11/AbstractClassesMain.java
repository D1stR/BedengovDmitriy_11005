import java.util.ArrayList;
public class AbstractClassesMain {
    public static void main(String[] args) {

        AbstractClassShape[] figures = new AbstractClassShape[5];
        figures[0] = new Rectangle(3, 3);
        figures[1] = new Circle(5);
        figures[2] = new Triangle(3, 3);
        figures[3] = new Rectangle(6,7);
        figures[4] = new Circle(8);
        ArrayList<String> figure = new ArrayList<String>();
        figure.add("Rectangle");
        figure.add("Circle");
        figure.add("Triangle");
        figure.add("Rectangle");
        figure.add("Circle");

        for (int i = 0; i < figures.length; i++){
            System.out.println("The " + figure.get(i)  +" with area: "+ figures[i].getArea());
        }
    }
}
