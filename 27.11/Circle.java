public class Circle extends AbstractClassShape {
    private int radius;

    Circle(int radius){
        this.radius = radius;
    }

    @Override
    double getArea() {
        return Math.PI*radius*radius;
    }
}