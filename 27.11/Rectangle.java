public class Rectangle extends AbstractClassShape {
    private int firstSideOfRectangle;
    private int secondSideOfRectangle;

    Rectangle(int firstSideOfRectangle, int secondSideOfRectangle){
        this.firstSideOfRectangle = firstSideOfRectangle;
        this.secondSideOfRectangle = secondSideOfRectangle;
    }

    @Override
    double getArea(){
        return firstSideOfRectangle*secondSideOfRectangle;
    }
}