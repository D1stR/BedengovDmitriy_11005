//Реализовать систему учёта покупателей/товаров/заказов. Должна храниться информация о покупателях (имя, возраст, пол), товарах (наименование, цена, производитель) и заказах (покупатель, купленный товар). Должна быть возможность вывести информацию о всех совершённых покупках. Должна быть возможность добавить новый заказ (указать покупателя и товар).
//Три класса: покупатель, товар, заказ.
//Информация вводится/выводится в любом удобном виде, лишь бы читаемо.
import java.util.Scanner;
import java.util.ArrayList;
public class AccountingSystem {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        Purchaser purchaser1 = new Purchaser("Petya", 21, "male");
        Purchaser purchaser2 = new Purchaser("Galina",22, "female");
        Purchaser purchaser3 = new Purchaser("Kolya",18,"male");

        Product mouse = new Product("Mouse G pro",30 , "Logitech");
        Product phone = new Product("Iphone 12",999 ,"Apple");
        Product monitor = new Product("ASUS ROG Strix XG27VQ,",2400, "Asus");

        Order order = new Order(purchaser1, mouse);
        Order order1 = new Order(purchaser2, phone);
        Order order2 = new Order(purchaser3, monitor);

        ArrayList<Order> allOreders = new ArrayList<>();
        allOreders.add(order);
        allOreders.add(order1);
        allOreders.add(order2);

        Purchaser[] purchasers = new Purchaser[3];
        purchasers[0] = purchaser1;
        purchasers[1] = purchaser2;
        purchasers[2] = purchaser3;

        Product[] products = new Product[3];
        products[0] = mouse;
        products[1] = phone;
        products[2] = monitor;

        System.out.println("Choose option: \n1. Add new order \n2. Watch orders");
        int n = in.nextInt();
        if (n == 1) {
            addNewOrder(purchasers, products, allOreders);
        } else if (n == 2){
            printOrders(allOreders);
        }
    }

    public static void printOrders(ArrayList<Order> allOreders){
        for (int i = 0; i < allOreders.size(); i++){
            System.out.println("order " + (i+1) + ": \n Purchaser is "+allOreders.get(i).getPurchaser() +
                    "\n Product is "+ allOreders.get(i).getProduct()  );
            System.out.println();
        }
    }

    public static void addNewOrder(Purchaser[] purchasers, Product[] products, ArrayList<Order> allOreders){
        Scanner in = new Scanner(System.in);
        System.out.println("Choose a purchaser: ");

        for (int i=0; i < purchasers.length;i++){
            System.out.println(i +  ". " + purchasers[i].getName());
        }
        int f = in.nextInt();

        System.out.println("Choose product: ");
        for (int i=0; i < products.length;i++){
            System.out.println(i +  ". " + products[i].getName());
        }
        int k = in.nextInt();

        Order newOrder = new Order(purchasers[f], products[k]);
        allOreders.add(newOrder);
        System.out.println("New order added: \n Purchaser is "+allOreders.get(allOreders.size()-1).getPurchaser() +
                "\n Product is "+ allOreders.get(allOreders.size()-1).getProduct() );
    }
}
