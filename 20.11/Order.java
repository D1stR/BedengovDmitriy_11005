public class Order {
    private Purchaser purchaser;
    private Product product;

    Order(Purchaser purchaser, Product product){
        this.purchaser = purchaser;
        this.product = product;
    }

    public String getProduct(){
        return this.product.getName();
    }

    public String getPurchaser(){
        return this.purchaser.getName();
    }
}
