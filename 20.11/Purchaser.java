import java.util.ArrayList;
public class Purchaser {
    private  String name;
    private int age;
    private String sex;

    Purchaser(String name, int age, String sex){
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

}
